package it.unifi.terreni;

import it.unifi.terreni.pattern.SimulatorFactory;
import it.unifi.terreni.simulator.Simulator;
import it.unifi.terreni.utils.EnumCase;

public class Main {


	public static void main(String[] args) throws Exception {
		SimulatorFactory mf = new SimulatorFactory(EnumCase.FIVE);

		Simulator sm = mf.createSimulator();

		while(!Simulator.isClosed()) {
			sm.nextStep();
//			Thread.sleep(10);
		}

	}

}
