package it.unifi.terreni.simulator;

import java.util.HashMap;
import java.util.Map;

import de.tudresden.sumo.cmd.Junction;
import de.tudresden.sumo.cmd.Simulation;
import de.tudresden.sumo.cmd.Vehicle;
import de.tudresden.sumo.config.Constants;
import de.tudresden.sumo.objects.SumoPosition2D;
import de.tudresden.sumo.objects.SumoStringList;
import de.tudresden.sumo.subscription.ResponseType;
import de.tudresden.sumo.subscription.SubscribtionVariable;
import de.tudresden.sumo.subscription.SubscriptionObject;
import de.tudresden.sumo.subscription.VariableSubscription;
import de.tudresden.sumo.util.Observable;
import de.tudresden.sumo.util.Observer;
import de.tudresden.sumo.util.SumoCommand;
import it.polito.appeal.traci.SumoTraciConnection;
import it.unifi.terreni.RBC;
import it.unifi.terreni.model.Train;
import it.unifi.terreni.utils.LoggerUtil;

public abstract class Simulator  implements Observer {
	protected static String SUMO_BIN = "sumo-gui";

	public static Integer START_TIME = 0;
	public static Integer STOP_TIME = 30000;

	protected static Map<String, Train> trains;

	protected static SumoTraciConnection conn = null;

	private Double stepLength;
	
	
	public Simulator() throws Exception {
		
	}

	public void connect(String configFile, String sumoBin) throws Exception {
		connect(configFile, sumoBin, 0.1);
	}


	public void connect(String configFile, String sumoBin, Double stepLength) throws Exception {
		this.stepLength = stepLength;
		RBC.getInstance().setStepLength(getStepLength());
		LoggerUtil.getInstance().createFile(this);
		System.out.println("Case:\t" + this.getPathName());
		conn = new SumoTraciConnection(sumoBin, configFile);
		conn.addOption("step-length", stepLength + "");
		conn.addOption("start", "true"); //start sumo immediately

		//start Traci Server
		conn.runServer();
		conn.setOrder(1);
		// conn.addObserver(new Simulator());

		trains = new HashMap<String, Train>();

		VariableSubscription vs = new VariableSubscription(SubscribtionVariable.simulation, START_TIME, STOP_TIME * 60, "");
		vs.addCommand(Constants.VAR_DEPARTED_VEHICLES_IDS);
		vs.addCommand(Constants.VAR_ARRIVED_VEHICLES_IDS);
		conn.do_subscription(vs);
	}

	public void createTrain(String vehID, String route, String depart, String departPosition, String departSpeed) throws Exception {
		conn.do_job_set(Vehicle.addFull(vehID, route, "train", depart == null ? "now" : depart, "0", departPosition == null ? "0" : departPosition, departSpeed == null ? "0" : departSpeed, "current", "max", "20", "", "", "", 100, 55));
	}
	
	public void removeTrain(String vehID) throws Exception {
		conn.do_job_set(Vehicle.remove(vehID, (byte) Constants.REMOVE_ARRIVED));
	}

	public void nextStep() throws Exception {
		conn.do_timestep();
		RBC.getInstance().tick();
		for (Map.Entry<String, Train> entry : trains.entrySet()) {
			Train t = entry.getValue();
			t.tick();
		}
	}

	public static Double getTime() throws Exception {
		return (Double) conn.do_job_get(Simulation.getTime());
	}

	public void update(Observable arg0, SubscriptionObject so) {
		if (so.response == ResponseType.SIM_VARIABLE) {
			if(so.variable == Constants.VAR_DEPARTED_VEHICLES_IDS) {
				SumoStringList ssl = (SumoStringList) so.object;
				if (ssl.size() > 0) {
					for (String vehID : ssl) {
						System.out.println("Departed vehicles: " + vehID);
						VariableSubscription vs = new VariableSubscription(SubscribtionVariable.vehicle, START_TIME, 100000 * 60, vehID);
						vs.addCommand(Constants.VAR_POSITION);
						vs.addCommand(Constants.VAR_SPEED);
						vs.addCommand(Constants.VAR_LANE_ID);
						try {
							trains.put(vehID, new Train(vehID, trains.isEmpty()));
							conn.do_subscription(vs);
						} catch (Exception ex) {
							ex.printStackTrace();
							System.err.println("subscription to " + vehID + " failed");
						}
					}
				}
			} else if(so.variable == Constants.VAR_ARRIVED_VEHICLES_IDS) {
				SumoStringList ssl = (SumoStringList) so.object;
				if (ssl.size() > 0) {
					for (String vehID : ssl) {
						System.out.println("Arrived vehicles: " + vehID);
						
						try {
							RBC.getInstance().remove(vehID);
							trains.remove(vehID);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				if(trains.isEmpty()) {
					try {
						
						System.out.println("End Simulation => time elapsed: \t" + getTime());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					Simulator.close();
				}
			}
		} else if (so.response == ResponseType.VEHICLE_VARIABLE) {
			Train t = trains.get(so.id);
			t.update(so);
			try {
				print();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void print() throws Exception {
		Double time = Simulator.getTime();
		for (Map.Entry<String, Train> entry : trains.entrySet()) {
			Train t = entry.getValue();
			StringBuffer sb = new StringBuffer()
					.append(time)
					.append(LoggerUtil.SEPARATOR)
					.append(t.getVehID())
					.append(LoggerUtil.SEPARATOR)
					.append(t.getActualSpeed())
					.append(LoggerUtil.SEPARATOR)
					.append(t.getPosition())
					.append(LoggerUtil.SEPARATOR)
					.append(t.getxPos())
					.append(LoggerUtil.SEPARATOR)
					.append(t.getyPos());
			LoggerUtil.getInstance().write(sb);
		}
	}

	public static void close() {
		conn.close();
	}

	public static void setSpeed(String vehID, double speed) throws Exception {
		conn.do_job_set(Vehicle.setSpeed(vehID, speed));
	}

	public static void setAcceleration(String vehID, double acc) throws Exception {
		conn.do_job_set(Vehicle.setAccel(vehID, acc));
	}
	
	public static void setDeceleration(String vehID, double decel) throws Exception {
		conn.do_job_set(Vehicle.setDecel(vehID, decel));
	}

	public static SumoPosition2D getJunctionPosition(String junctionID) throws Exception {
		return (SumoPosition2D) conn.do_job_get(Junction.getPosition(junctionID));
	}

	public Double getStepLength() {
		return stepLength;
	}

	public void setStepLength(Double stepLength) {
		this.stepLength = stepLength;
	}

	public abstract void config() throws Exception;

	public static boolean isClosed() {
		return conn.isClosed();
	}
	
	public static Object doJob(SumoCommand sm) throws Exception {
		return conn.do_job_get(sm);
	}
	
	public abstract String getPathName();
}
