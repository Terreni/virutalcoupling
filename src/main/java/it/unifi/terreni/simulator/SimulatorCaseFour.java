package it.unifi.terreni.simulator;

import java.math.BigDecimal;
import java.util.Random;

import org.oristool.simulator.samplers.ExponentialSampler;

import it.unifi.terreni.model.Train;

public class SimulatorCaseFour extends Simulator {
	private static String FILE_MODEL = "src\\main\\resources\\case4\\test1.sumocfg";
	private static double LAMBDA = 0.005;
	private String vhLostConnId;
	private double vhLostConnDuration;
	private boolean disconnectionSend = false;
	private double timeDisconnection;

	public SimulatorCaseFour() throws Exception {
		super();
	}

	private void connect() throws Exception {
		super.connect(FILE_MODEL, SUMO_BIN);
		conn.addObserver(new SimulatorCaseFour());
	}

	private void createTrain() throws Exception {
		super.createTrain("TrainA", "route_0", "now", "0", "20");
		super.createTrain("TrainB", "route_0", "60", "0", "10");
		super.createTrain("TrainC", "route_0", "120", "0", "10");
		super.createTrain("TrainD", "route_0", "180", "0", "10");
		
		this.vhLostConnId = this.connectionLostTrainId("TrainA", "TrainB", "TrainC", "TrainD" );
		this.vhLostConnDuration = this.getExpSample();
		this.timeDisconnection = this.getExpSample() + 180.0;
		
		System.out.println("Train that will go to disconnect: " + vhLostConnId + "\tDuration of disconnection: " + vhLostConnDuration + "\tAt time: " + this.timeDisconnection);
	}

	@Override
	public void config() throws Exception {
		connect();
		createTrain();
	}
	@Override
	public String getPathName() {
		return "case4";
	}

	@Override
	public void nextStep() throws Exception {
		super.nextStep();

		if(!disconnectionSend && trains.get(this.vhLostConnId) != null) {
			Train t = trains.get(this.vhLostConnId);
			if(getTime() >= timeDisconnection) {
				disconnectionSend = true;
				System.out.println(t.getVehID() + " lost connecction at time:\t" + getTime());
				t.setDisconnectionDuration(vhLostConnDuration);
				t.sendDisconnectionRequest();
			}
		}
	}

	private String connectionLostTrainId(String... vehID) {
		Random rd = new Random();
		return vehID[rd.nextInt(vehID.length)];
	}
	
	private double getExpSample() {
		ExponentialSampler exp = new ExponentialSampler(new BigDecimal(LAMBDA));
		return exp.getSample().doubleValue();
	}
	
}
