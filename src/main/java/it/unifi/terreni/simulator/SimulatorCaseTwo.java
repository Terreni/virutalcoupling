package it.unifi.terreni.simulator;

import de.tudresden.sumo.cmd.Simulation;

public class SimulatorCaseTwo extends Simulator {
	private static String FILE_MODEL = "src\\main\\resources\\case2\\test1.sumocfg";
	public SimulatorCaseTwo() throws Exception {
		super();
	}
	
	private void connect() throws Exception {
		super.connect(FILE_MODEL, SUMO_BIN);
		conn.addObserver(new SimulatorCaseTwo());
	}
	
	private void createTrain() throws Exception {
		super.createTrain("TrainA", "route_0", "now", "0", "20");
		super.createTrain("TrainB", "route_0", "60", "0", "10");
		super.createTrain("TrainC", "route_0", "120", "0", "10");
		super.createTrain("TrainD", "route_0", "180", "0", "10");
	}

	@Override
	public void config() throws Exception {
		connect();
		createTrain();
	}
	
	
	@Override
	public void nextStep() throws Exception {
		super.nextStep();
		Double timeSeconds = (Double) conn.do_job_get(Simulation.getTime());
		if(timeSeconds > 800.0 && timeSeconds < 1000.0) {
			super.setSpeed("TrainA", 10);
		}
	}
	@Override
	public String getPathName() {
		return "case2";
	}

}
