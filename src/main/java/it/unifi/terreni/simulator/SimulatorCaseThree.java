package it.unifi.terreni.simulator;

import it.unifi.terreni.RBC;

public class SimulatorCaseThree extends Simulator {
	private static String FILE_MODEL = "src\\main\\resources\\case3\\test1.sumocfg";
	
	public SimulatorCaseThree() throws Exception {
		super();
	}
	
	private void connect() throws Exception {
		super.connect(FILE_MODEL, SUMO_BIN);
		conn.addObserver(new SimulatorCaseThree());
		RBC.getInstance().activateJunction();
	}
	
	private void createTrain() throws Exception {
		super.createTrain("TrainA", "route_1", "now", "0", "20");
		super.createTrain("TrainB", "route_0", "60", "0", "10");
	}

	@Override
	public void config() throws Exception {
		connect();
		createTrain();
	}
	
	@Override
	public String getPathName() {
		return "case3";
	}


}
