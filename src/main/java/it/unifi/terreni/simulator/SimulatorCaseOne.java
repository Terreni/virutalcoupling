package it.unifi.terreni.simulator;

public class SimulatorCaseOne extends Simulator {
	private static String FILE_MODEL = "src\\main\\resources\\case1\\test1.sumocfg";
	
	public SimulatorCaseOne() throws Exception {
		super();
	}
	
	private void connect() throws Exception {
		super.connect(FILE_MODEL, SUMO_BIN);
		conn.addObserver(new SimulatorCaseOne());
		
	}
	
	private void createTrain() throws Exception {
		super.createTrain("TrainA", "route_0", "now", "0", "20");
		super.createTrain("TrainB", "route_0", "60", "0", "10");
//		super.createTrain("TrainC", "route_0", "120", "0", "10");
//		super.createTrain("TrainD", "route_0", "180", "0", "10");
	}

	@Override
	public void config() throws Exception {
		connect();
		createTrain();
	}

	@Override
	public String getPathName() {
		return "case1";
	}

}
