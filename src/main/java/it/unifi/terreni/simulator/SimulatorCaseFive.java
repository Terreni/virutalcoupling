package it.unifi.terreni.simulator;

import java.math.BigDecimal;
import java.util.Random;

import org.oristool.simulator.samplers.ExponentialSampler;

import it.unifi.terreni.model.Train;

public class SimulatorCaseFive extends Simulator {
	private static String FILE_MODEL = "src\\main\\resources\\case5\\test1.sumocfg";
	private static double LAMBDA = 0.005;
	
	private static Integer UPPER_BOUND = 10000;
	private static Integer theshold = 10;

	private Double timeToDisconnect;
	private static String trainIds[] = {"TrainA","TrainB","TrainC","TrainD"};
	private String vhId;
	private Boolean toBeDisconnect;
	private static int numTrain = 4;

	public SimulatorCaseFive() throws Exception {
		super();

		timeToDisconnect = .0;
		toBeDisconnect = false;
	}

	private void connect() throws Exception {
		super.connect(FILE_MODEL, SUMO_BIN);
		conn.addObserver(new SimulatorCaseFour());
	}

	private void createTrain() throws Exception {
		super.createTrain("TrainA", "route_0", "now", "0", "20");
		super.createTrain("TrainB", "route_0", "60", "0", "10");
//		super.createTrain("TrainC", "route_0", "120", "0", "10");
//		super.createTrain("TrainD", "route_0", "180", "0", "10");

	}

	@Override
	public void config() throws Exception {
		connect();
		createTrain();
	}
	@Override
	public String getPathName() {
		return "case5";
	}

	@Override
	public void nextStep() throws Exception {
		super.nextStep();

//		double now = getTime();

//		if(!toBeDisconnect && connectionLost()) {
//			toBeDisconnect = true;
//			int index = connectionLostIndex();
//			vhId = trainIds[index];
//			timeToDisconnect = now + getExpSample();
//			System.out.println(vhId + " will disconnect at time " + timeToDisconnect);
//		}

//		if(toBeDisconnect && now > timeToDisconnect && trains.get(vhId) != null) {
//			toBeDisconnect = false;
//			Train t = trains.get(vhId);
//			double vhLostConnDuration = getExpSample();
//			System.out.println(t.getVehID() + " lost connecction at time:\t" + getTime() + "\t Duration: " + vhLostConnDuration);
//			t.setDisconnectionDuration(vhLostConnDuration);
//			t.sendDisconnectionRequest();
//		}

	}

	private double getExpSample() {
		ExponentialSampler exp = new ExponentialSampler(new BigDecimal(LAMBDA));
		return exp.getSample().doubleValue();
	}

	private int connectionLostIndex() {
		Random rd = new Random();
		return rd.nextInt(numTrain);
	}

	private boolean connectionLost() {
		Random rd = new Random();
		int vlaue = rd.nextInt(UPPER_BOUND);
		return vlaue < theshold;
	}
}
