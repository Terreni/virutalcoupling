package it.unifi.terreni;

import it.unifi.terreni.model.BaseStation;
import it.unifi.terreni.model.TestTrain;
import it.unifi.terreni.utils.PropertiesUtils;
import it.unifi.terreni.utils.TimeStampUtils;

public class TestConnection {

	public static void main(String[] args) throws Exception {
		//		double l = 0.05;
		//		List<Double> arr = new ArrayList<Double>();
		//		for (int i = 0; i < 35; i++) {
		//			System.out.println(l);
		//			double media = 0;
		//			double max = -100;
		//			double min = 9999999;
		//			arr.clear();
		//			for (int j = 0; j < 1000; j++) {
		//				double v = getExpSample(l);
		//				arr.add(v);
		//				if(v > max) max = v;
		//				if(v < min) min = v;
		//				media += v;
		//			}
		//			media = media / 1000;
		//			double varianza = getVarianza(arr, media);
		//			System.out.println("Max: " + max + "\tMin: " + min + "\tMedia: " + (media) + "\tVarianza: " + (varianza));
		//			System.out.println("##########################################################");
		//			l += 0.05;
		//		}		
		TestTrain ta = new TestTrain("TrainA", "TrainB");
		TestTrain tb = new TestTrain("TrainB", true);
		while(TimeStampUtils.getInstance().getMillis() <= PropertiesUtils.SIM_DURATION) {
			ta.tick();
			tb.tick();
			BaseStation.getInstance().tick();
			TimeStampUtils.getInstance().tick();
		}			
	}

	//	private static double getVarianza(List<Double> arr, double media) {
	//		double var = 0;
	//		for (Double d : arr) {
	//			var += Math.pow(d - media, 2) ;
	//		}
	//		return var / 1000;
	//	}
	//	private static double getExpSample(double lambda) {
	//		ExponentialSampler exp = new ExponentialSampler(new BigDecimal(lambda));
	//		return exp.getSample().doubleValue();
	//	}

}
