package it.unifi.terreni.queue;

import java.util.LinkedList;

import it.unifi.terreni.model.Train;

public class Queue {
	private LinkedList<Trains> list;

	public Queue() {
		this.list = new LinkedList<Trains>();
	}

	public void clear() {
		this.list.clear();
	}
	
	public boolean isEmpty() {
		return this.list.isEmpty();
	}
	
	public void enQueue(Trains t) {
		this.list.add(t);
	}

	public Trains deQueue() {
		return this.list.remove();
	}

	public LinkedList<Trains> getList() {
		return this.list;
	}

	public Trains lastElement() {
		return this.list.getLast();
	}
	public Trains firstElement() {
		return this.list.getFirst();
	}
	

	public Train findHeadTrains(Train t) {
		for (Trains ts : list) {
			if(t.equals(ts.getTrain())) {
				return ts.getNextTrain();
			}
		}
		return null;
	}
	
	public static class Trains{
		private Train nextTrain;
		private Train train;
		
		public void setHead() {
			this.nextTrain = null;
		}

		public Train getNextTrain() {
			return nextTrain;
		}
		public Train getTrain() {
			return train;
		}
		private Trains(Builder builder) {
			this.nextTrain = builder.nextTrain;
			this.train = builder.train;
		}
		public static Builder builder() {
			return new Builder();
		}
		public static final class Builder {
			private Train nextTrain;
			private Train train;

			private Builder() {
			}

			public Builder nextTrain(Train nextTrain) {
				this.nextTrain = nextTrain;
				return this;
			}

			public Builder train(Train train) {
				this.train = train;
				return this;
			}

			public Trains build() {
				return new Trains(this);
			}
		}


	}

}
