package it.unifi.terreni.utils;

public class Analysis {

	private Integer numDataSend;
	private Integer numDataLost;
	private Integer numAckSend;
	private Integer numAckLost;

	private Integer numTotSend;
	private Integer numTotLost;

	private Long diffTime;
	private Long diffTimeData;
	private Long diffTimeAck;

	public Analysis() {
		this.numDataSend = 0;
		this.numDataLost = 0;
		this.numAckSend = 0;
		this.numAckLost = 0;
		this.numTotSend = 0;
		this.numTotLost = 0;
		this.diffTimeData = 0l;
		this.diffTimeAck = 0l;
		this.diffTime = 0l;
	}
	
	
	public void incSend(MessageType m, Long diff) {
		if(MessageType.DATA.equals(m)) {
			incDataSend();
			incDifTimeData(diff);
		} else {
			incAckSend();
			incDifTimeAck(diff);
		}
		
		incDifTime(diff);
	}
	
	
	public void incLost(MessageType m, Long diff) {
		if(MessageType.DATA.equals(m)) {
			incDataLost();
			incDifTimeData(diff);
		} else {
			incAckLost();
			incDifTimeAck(diff);
		}
		
		incDifTime(diff);

	}
	
	
	private void incDataSend() {
		this.numDataSend++;
		this.numTotSend++;
	}
	private void incAckSend() {
		this.numAckSend++;
		this.numTotSend++;
	}
	private void incDataLost() {
		this.numDataLost++;
		this.numTotLost++;
	}
	private void incAckLost() {
		this.numAckLost++;
		this.numTotLost++;
	}



	public Double getAvgDataSend() {
		return ((double) numDataSend) / ((double) numTotSend);
	}



	public Double getAvgDataLost() {
		return ((double) numDataLost) / ((double) numTotLost);
	}



	public Double getAvgAckSend() {
		return ((double) numAckSend) / ((double) numTotSend);
	}



	public Double getAvgAckLost() {
		return ((double) numAckLost) / ((double) numTotLost);
	}



	public Double getAvgTimeData() {
		Integer num = numDataSend + numDataLost;
		return diffTimeData / ((double) num);
	}



	public Double getAvgTimeAck() {
		Integer num = numAckSend + numAckLost;
		return diffTimeAck / ((double) num);
	}
	
	
	public Double getAvgTime() {
		Integer num = numDataSend + numDataLost + numAckSend + numAckLost;
		return diffTime / ((double) num);
	}
	
	
	private void incDifTime(Long diff) {
		this.diffTime += diff;
	}
	
	private void incDifTimeData(Long diff) {
		this.diffTimeData += diff;
	}
	
	private void incDifTimeAck(Long diff) {
		this.diffTimeAck += diff;
	}
	
	
	@Override
	public String toString() {
		
		return (new StringBuilder())
				.append("AvgDataSend:")
				.append("\t")
				.append(getAvgDataSend())
				.append("\t")
				.append("AvgDataLost:")
				.append("\t")
				.append(getAvgDataLost())
				.append("\t")
				.append("AvgAckSend:")
				.append("\t")
				.append(getAvgAckSend())
				.append("\t")
				.append("AvgAckLost:")
				.append("\t")
				.append(getAvgAckLost())
				.append("\t")
				.append("\t")
				.append("#DataSend")
				.append("\t")
				.append(numDataSend)
				.append("\t")
				.append("#DataLost")
				.append("\t")
				.append(numDataLost)
				.append("\t")
				.append("#AckSend")
				.append("\t")
				.append(numAckSend)
				.append("\t")
				.append("#AckLost")
				.append("\t")
				.append(numAckLost)
				.append("\t")
				.append("#TotSend")
				.append("\t")
				.append(numTotSend)
				.append("\t")
				.append("#TotLost")
				.append("\t")
				.append(numTotLost)
				.append("\t")
				.append("AvgTimeData")
				.append("\t")
				.append(getAvgTimeData())
				.append("\t")
				.append("AvgTimeAck")
				.append("\t")
				.append(getAvgTimeAck())
				.append("\t")
				.append("AvgTime")
				.append("\t")
				.append(getAvgTime())
				.append("\t")
				
				.toString();
	}



}
