package it.unifi.terreni.utils;

import it.unifi.terreni.RBC;

public class MathTrain {
	public static Double getTimeLinearAcceleration(Double v1, Double v2, Double a) {
		return v2 > v1 ? (v2 - v1) / a : (v1 - v2) / a;
	}
	
	public static Double getTimeLinearVelocity(Double x, Double v) {
		return (v == 0 ? .0 : x / v);
	}
	
	public static Double getPosLinearAcceleration(Double a, Double v, Double t) {
		return (0.5 * a * Math.pow(t, 2)) + (v * t);
	}
	
	public static Double getPosLinearVelocity(Double v, Double t) {
		return (v * t);
	}

	public static Double getMinimalDistance(Double posA, Double posB) {
		return Math.abs(posB - (posA - RBC.minDistance));
	}
	
	
	public static Double getDistance(Double x1, Double y1, Double x2, Double y2) {
		if(x1 == null || y1 == null || x2 == null || y2 == null) return .0;
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}
}
