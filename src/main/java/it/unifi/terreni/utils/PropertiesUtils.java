package it.unifi.terreni.utils;

public class PropertiesUtils {
	public static Long UPDATE_TIME = 200l; //200ms
	public static Long MESSAGE_LOST = 1000l; //se dopo 5 tentativi 200ms * 5 = 1000ms non si riceve messaggio errore! 
	public static Long TICK_DURATION = 1l;
	public static Long SIM_DURATION = 1200000l; //5min
//	public static Long SIM_DURATION = 1000l; //1sec
}
