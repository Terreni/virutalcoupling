package it.unifi.terreni.utils;

public class TimeStampUtils {
	private static TimeStampUtils instance;
	private long millis;
	
	private TimeStampUtils() {
		this.millis = 0;
	}
	
	public static TimeStampUtils getInstance() {
		if(instance == null) {
			instance = new TimeStampUtils();
		}
		
		return instance;
	}
	
	public long getMillis() {
		return millis;
	}
	
	public void tick() throws InterruptedException {
		millis++;
		//Thread.sleep(1);
	}
	
	public long getMillis(long val) {
		return millis + val;
	}
	
}
