package it.unifi.terreni.utils;

public enum MessageType {
	DATA,
	ACK;
}
