package it.unifi.terreni.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import it.unifi.terreni.model.BaseStation;
import it.unifi.terreni.simulator.Simulator;

public class LoggerUtil {
	public static String SEPARATOR = ",";
	private static LoggerUtil instance;
	private String fileName;

	private LoggerUtil() {

	}

	private String getPath(Simulator s) {
		return s.getPathName();
	}

	private void writeHeader() {
		StringBuilder sb = new StringBuilder()
				.append("time")
				.append(SEPARATOR)
				.append("train")
				.append(SEPARATOR)
				.append("speed")
				.append(SEPARATOR)
				.append("distance")
				.append(SEPARATOR)
				.append("x")
				.append(SEPARATOR)
				.append("y");

		write(sb.toString());
	}

	public static LoggerUtil getInstance() {
		if(instance == null) {
			instance = new LoggerUtil();
		}

		return instance;
	}

	public void createFile(Simulator simulator) {
		try {
			String directoryName = "simulation/" + getPath(simulator);
			fileName = directoryName + "/sim-" + System.currentTimeMillis() + ".csv";

			File myObj = new File(fileName);
			File directory = new File(directoryName);
			if (! directory.exists()){
				directory.mkdir();
				// If you require it to make the entire directory path including parents,
				// use directory.mkdirs(); here instead.
			}
			myObj.createNewFile();
			writeHeader();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void createFile(BaseStation baseStations, ChannelParameter chP) {
		try {
			String directoryName = "simulation/base_station/" + chP.getPath();
			fileName = directoryName + "/sim-" + System.currentTimeMillis() + ".csv";

			File myObj = new File(fileName);
			File directory = new File(directoryName);
			if (! directory.exists()){
				directory.mkdirs();
				// If you require it to make the entire directory path including parents,
				// use directory.mkdirs(); here instead.
			}
			myObj.createNewFile();
			writeBaseStationHeader();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeBaseStationHeader() {
		StringBuilder sb = new StringBuilder()
				.append("sender")
				.append(SEPARATOR)
				.append("receiver")
				.append(SEPARATOR)
				.append("counter")
				.append(SEPARATOR)
				.append("MessageType")
				.append(SEPARATOR)
				.append("time")
				.append(SEPARATOR)
				.append("lost")
				.append(SEPARATOR)
				.append("timestamp");

		write(sb.toString());
	}

	public void  write(String data) {
		try {
			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(data);
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	public void write(StringBuffer sb) {
		this.write(sb.toString());
	}
}
