package it.unifi.terreni.utils;

public enum ChannelParameter {
	WORSE(0.01, 100l, 1l, 0.2, "worse"),
	MEDIUM(0.001, 10l, 10l, 0.3, "medium"),
	BEST(0.0001, 1l, 100l, 2.0, "best");
	
	private Long delay;
	private Long bitRate;
	private Double lambda;
	private DistributedRandomNumberGenerator g;
	private String path;
	
	private ChannelParameter(Double lost, Long delay, Long bitRate, Double lambda, String path) {
		this.delay = delay;
		this.bitRate = bitRate;
		this.path = path;
		this.lambda = lambda;
		g = new DistributedRandomNumberGenerator();
		g.addNumber(1, lost);
		g.addNumber(0, 1 - lost);
		
	}

	public Long getDelay() {
		return delay;
	}
	

	public Double getLambda() {
		return lambda;
	}

	public Long getBitRate() {
		return bitRate;
	}
	
	public Boolean isLost() {
		return g.getDistributedRandomNumber() == 1;
	}
	
	public String getPath() {
		return path;
	}
}
