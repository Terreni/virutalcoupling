package it.unifi.terreni.pattern;

import it.unifi.terreni.simulator.*;
import it.unifi.terreni.utils.EnumCase;

public class SimulatorFactory {

	EnumCase enumCase;

	public SimulatorFactory(EnumCase enumCase) {
		this.enumCase = enumCase;
	}

	public Simulator createSimulator() throws Exception {

		Simulator sm = null;

		if(EnumCase.ONE.equals(enumCase)) {
			sm = new SimulatorCaseOne();
		} else if(EnumCase.TWO.equals(enumCase)) {
			sm = new SimulatorCaseTwo();
		} else if(EnumCase.THREE.equals(enumCase)) {
			sm = new SimulatorCaseThree();
		} else if(EnumCase.FOUR.equals(enumCase)) {
			sm = new SimulatorCaseFour();
		} else if(EnumCase.FIVE.equals(enumCase)) {
			sm = new SimulatorCaseFive();
		}

		sm.config();

		return sm;


	}
}
