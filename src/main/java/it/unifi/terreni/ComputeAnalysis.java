package it.unifi.terreni;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import it.unifi.terreni.utils.Analysis;
import it.unifi.terreni.utils.MessageType;

public class ComputeAnalysis {
	public static String path = "simulation/base_station/";
	
	public ComputeAnalysis() {
	}
	
	public static void main(String[] args) throws IOException {
		ComputeAnalysis ca = new ComputeAnalysis();
		System.out.println("\n\nWorse\n\n");
		File folder = new File(path + "worse/");
		ca.listFilesForFolder(folder);
		
		System.out.println("\n\nMedium\n\n");
		folder = new File(path + "medium/");
		ca.listFilesForFolder(folder);

		System.out.println("\n\nBest\n\n");
		folder = new File(path + "best/");
		ca.listFilesForFolder(folder);
	}

	public void listFilesForFolder(final File folder) throws IOException {
		File[] listOfFiles = folder.listFiles();
		Analysis analysis = new Analysis();
		for (File file : listOfFiles) {
			if (file.isFile() && file.getName().endsWith(".csv")) {
				readFromInputStream(file, analysis);
			}
		}
		
		System.out.println(analysis.toString());
	}


	private void readFromInputStream(File file, Analysis analysis) throws IOException {
		StringBuffer sb = new StringBuffer();
		
		boolean firstLine = true;
		List<CSV> csv = new ArrayList<ComputeAnalysis.CSV>();
		try {
			Scanner myReader = new Scanner(file);
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				sb.append(data);
				sb.append("\n");
				if(!firstLine) {
					csv.add(new CSV(data.split(",")));
				}
				firstLine = false;
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		computeAnalysis(csv, analysis);
		
		
	}
	
	private void computeAnalysis(List<CSV> array, Analysis analysis) {
		
		for (CSV csv : array) {
			if(csv.getLost()) {
				analysis.incLost(csv.getType(), csv.getTimeReceived() - csv.getTimeSend());
			} else {
				analysis.incSend(csv.getType(), csv.getTimeReceived() - csv.getTimeSend());
			}
			
		}
		
		
		
	}
	
	public class CSV {
		private MessageType type;
		private Long timeReceived;
		private Long timeSend;
		private Boolean lost;
		
		public CSV(String[] data) {
			this.type = "DATA".equals(data[3]) ? MessageType.DATA : MessageType.ACK;
			this.timeReceived = Long.valueOf(data[6]);
			this.timeSend = Long.valueOf(data[4]);
			this.lost = Integer.valueOf(data[5]) == 1;
		}

		public MessageType getType() {
			return type;
		}

		public Long getTimeReceived() {
			return timeReceived;
		}

		public Boolean getLost() {
			return lost;
		}

		public Long getTimeSend() {
			return timeSend;
		}
		
		
		
	}
}
