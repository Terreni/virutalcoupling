package it.unifi.terreni.model;

import it.unifi.terreni.utils.MessageType;
import it.unifi.terreni.utils.PropertiesUtils;
import it.unifi.terreni.utils.TimeStampUtils;

public class TestTrain implements BaseTrain{
	private TrainData td;
	private String vhConnected;
	private Long lastMessage;
	private boolean isVirtualCoupled;
	private Long previousMillis;
	private Integer msgCounter;
	private Boolean ackReceived;

	public TestTrain(String vehID, boolean isVirtualCoupled) {
		init(vehID, null, false);
	}

	public TestTrain(String vehID, String vhConnected) {
		init(vehID, vhConnected, isVirtualCoupled);
	}

	private void init(String vehID, String vhConnected, boolean isVirtualCoupled) {
		this.lastMessage = 0l;
		this.previousMillis = 0l;
		this.ackReceived = true;
		this.msgCounter = 0;
		td = new TrainData(vehID);
		BaseStation.getInstance().connect(this);
		this.vhConnected = vhConnected;
		this.isVirtualCoupled = isVirtualCoupled;
	}

	public void nextTrainInfo(Object obj) throws Exception {
		Message msg = (Message) obj;
		if(MessageType.DATA.equals(msg.getMsgType())) {
			this.lastMessage = TimeStampUtils.getInstance().getMillis();
			//se data invio l'ack
			BaseStation.getInstance().sendMessage(getAck(msg));
		} else if(MessageType.ACK.equals(msg.getMsgType()) && msg.getCounter() == msgCounter){ // se non � data � ack
			this.ackReceived = true;
		}

	}

	public void tick() throws Exception {
		long current = TimeStampUtils.getInstance().getMillis();
		if(current - previousMillis >= PropertiesUtils.UPDATE_TIME) {//200ms
			previousMillis = current;
			if(this.isConnect()) {
				if(!this.ackReceived) {
					System.out.println("Ack not received");
				}
				this.ackReceived = false;
				this.msgCounter++; //se ack incremento contatore messaggi
				BaseStation.getInstance().sendMessage(getMessage(vhConnected, null));
			} 
		}


		if(isVirtualCoupled && checkLastMessageLost()) {
			System.out.println("Connection Error");
		}

	}
	
	public String getVehID() {
		return td.getVehID();
	}

	public void sendConnectionRequest(String vhConnected) {
		this.vhConnected = vhConnected;
	}

	public void sendDisconnectionRequest() {
		this.vhConnected = null;
	}

	public boolean isConnect() {
		return this.vhConnected != null;
	}

	private boolean checkLastMessageLost() {
		return Math.abs(TimeStampUtils.getInstance().getMillis() - lastMessage) >= PropertiesUtils.MESSAGE_LOST;
	}

	@Override
	public String toString() {
		return this.getVehID();
	}

	private Message getAck(Message msg) {
		return Message.builder()
				.sender(msg.getReceiver())
				.receiver(msg.getSender())
				.counter(msg.getCounter())
				.timestamp(TimeStampUtils.getInstance().getMillis())
				.msgType(MessageType.ACK)
				.body(null)
				.build();
	}


	private Message getMessage(String receiver, Object body) {
		return Message.builder()
				.sender(getVehID())
				.receiver(receiver)
				.counter(msgCounter)
				.timestamp(TimeStampUtils.getInstance().getMillis())
				.msgType(MessageType.DATA)
				.body(body)
				.build();
	}

}
