package it.unifi.terreni.model;

public interface BaseTrain {
	public void nextTrainInfo(Object td) throws Exception;
	public void tick() throws Exception;
	public void sendDisconnectionRequest();
	public String getVehID();
}	
