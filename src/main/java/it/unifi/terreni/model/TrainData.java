package it.unifi.terreni.model;

import it.unifi.terreni.simulator.Simulator;

public class TrainData {
	private Double actualSpeed;
	private Double xPos;
	private Double yPos;
	private String vehID;
	private String laneId;
	private String endRouteId;
	private Double disconnectionDuration;
	private String destTrainInfo;

	
	public TrainData(String vehID) {
		this.vehID = vehID;
		this.destTrainInfo = null;
		actualSpeed = .0;
		laneId = "";
	}
	
	public Double getActualSpeed() {
		return actualSpeed;
	}
	public void setActualSpeed(Double actualSpeed) {
		this.actualSpeed = actualSpeed;
	}
	public Double getxPos() {
		return xPos;
	}
	public void setxPos(Double xPos) {
		this.xPos = xPos;
	}
	public Double getyPos() {
		return yPos;
	}
	public void setyPos(Double yPos) {
		this.yPos = yPos;
	}
	public String getVehID() {
		return vehID;
	}
	public void setVehID(String vehID) {
		this.vehID = vehID;
	}

	public String getLaneId() {
		return laneId;
	}

	public void setLaneId(String laneId) {
		this.laneId = laneId;
	}

	public String getEndRouteId() {
		return endRouteId;
	}

	public void setEndRouteId(String endRouteId) {
		this.endRouteId = endRouteId;
	}

	public Double getDisconnectionDuration() {
		return disconnectionDuration;
	}

	public void setDisconnectionDuration(Double disconnectionDuration) {
		this.disconnectionDuration = disconnectionDuration;
	}
	
	public boolean isConnected() {
//		return disconnectionDuration == null || disconnectionDuration <= .0;
		return BaseStation.getInstance().isConnect(vehID);
	}

	
	public String getDestTrainInfo() {
		return destTrainInfo;
	}

	public void setDestTrainInfo(String destTrainInfo) {
		this.destTrainInfo = destTrainInfo;
	}

	public void reduceDisconnection(Double stepLength) throws Exception {
		if(disconnectionDuration == null) return;
		this.disconnectionDuration -= stepLength;
		if(disconnectionDuration <= .0) {
			System.out.println(this.getVehID() + " reconnect at time " + Simulator.getTime());
		}
	}
}
