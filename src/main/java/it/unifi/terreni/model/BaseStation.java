package it.unifi.terreni.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.oristool.simulator.samplers.ExponentialSampler;

import it.unifi.terreni.utils.ChannelParameter;
import it.unifi.terreni.utils.LoggerUtil;
import it.unifi.terreni.utils.TimeStampUtils;

public class BaseStation {
	private static BaseStation instance;
	private Map<String, BaseTrain> connectedTrain;
	Queue<Message> queue;
	private ChannelParameter chP;
	
	private BaseStation() {
		this.connectedTrain = new HashMap<String, BaseTrain>();
		this.queue = new LinkedList<Message>();
		chP = ChannelParameter.BEST;
		LoggerUtil.getInstance().createFile(instance, chP);
	}

	public static BaseStation getInstance() {
		if(instance == null) {
			instance = new BaseStation();
		}

		return instance;
	}

	public void sendMessage(Message message) throws Exception {
		message.setEffectiveTime(addDelay());
		queue.add(message);
	}

	public boolean isConnect(String vehID) {
		return connectedTrain.containsKey(vehID);
	}

	public void connect(BaseTrain t) {
		connectedTrain.put(t.getVehID(), t);
	}

	public void disconnect(BaseTrain t) {
		connectedTrain.remove(t.getVehID());
	}

	public void writeLog(Message m) {
		System.out.println(m.toString());
		LoggerUtil.getInstance().write((new StringBuilder())
				.append(m.toCsv())
				.append(LoggerUtil.SEPARATOR)
				.append(TimeStampUtils.getInstance().getMillis())
				.toString());
	}

	public void tick() throws Exception {
		if(queue.isEmpty()) return;

		Queue<Message> temp = new LinkedList<Message>();

		while(!queue.isEmpty()) {
			Message msg = queue.poll();
			Long current = TimeStampUtils.getInstance().getMillis();
			if(current >= msg.getEffectiveTime()) {
				if(chP.isLost()) {
					msg.setLost(1);
				} else {
					connectedTrain.get(msg.getReceiver()).nextTrainInfo(msg);
				}


				writeLog(msg);
			} else { 
				temp.add(msg);
			}
		}

		queue.clear();

		while(!temp.isEmpty()) {
			queue.add(temp.poll());
		}

	}

	private Long addDelay() {
		double exp = getExpSample();
		exp = Math.random() > 0.5 ?  exp : (-1) * exp;
		Long expl = chP.getDelay() + (long) exp;
		return TimeStampUtils.getInstance().getMillis(expl > 0 ? expl : 0);
	}

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();
		strb.append("Train Connected\t");
		for (Map.Entry<String, BaseTrain> value : connectedTrain.entrySet()) {
			strb.append(value.getKey())
			.append("\t");
		}

		return strb.toString();
	}



	private double getExpSample() {
		ExponentialSampler exp = new ExponentialSampler(new BigDecimal(chP.getLambda()));
		return exp.getSample().doubleValue();
	}
}
