package it.unifi.terreni.model;

import it.unifi.terreni.utils.LoggerUtil;
import it.unifi.terreni.utils.MessageType;

public class Message {
	private String sender;
	private String receiver;
	private Integer counter;
	private Long timestamp;
	private Long effectiveTime;
	private Integer lost;
	private MessageType msgType;
	private Object body;


	private Message(Builder builder) {
		this.sender = builder.sender;
		this.receiver = builder.receiver;
		this.msgType = builder.msgType;
		this.body = builder.body;
		this.counter = builder.counter;
		this.lost = 0;
		this.timestamp = builder.timestamp;
		this.effectiveTime = builder.effectiveTime;
	}
	public static Builder builder() {
		return new Builder();
	}


	public String getSender() {
		return sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public MessageType getMsgType() {
		return msgType;
	}
	public Object getBody() {
		return body;
	}

	public Integer getCounter() {
		return counter;
	}
	
	public Long getTimestamp() {
		return timestamp;
	}
	
	public Integer getLost() {
		return lost;
	}
	public void setLost(Integer lost) {
		this.lost = lost;
	}
	
	public Long getEffectiveTime() {
		return effectiveTime;
	}
	public void setEffectiveTime(Long effectiveTime) {
		this.effectiveTime = effectiveTime;
	}
	
	@Override
	public String toString() {
		return (new StringBuilder())
				.append("Sender:")
				.append("\t")
				.append(sender)
				.append("\t")
				.append("Receiver:")
				.append("\t")
				.append(receiver)
				.append("\t")
				.append("Counter:")
				.append("\t")
				.append(counter)
				.append("\t")
				.append("Type:")
				.append("\t")
				.append(MessageType.DATA.equals(msgType) ? "DATA" : "ACK")
				.append("\t")
				.append("Time:")
				.append("\t")
				.append(timestamp)
				.append("\t")
				.append("ETime:")
				.append("\t")
				.append(effectiveTime)
				.append("\t")
				.append("diff:")
				.append("\t")
				.append(effectiveTime - timestamp)
				.append("\t")
				.append("Lost:")
				.append("\t")
				.append(lost)
				.append("\t")
				.toString();
	}
	
	public String toCsv() {
		return (new StringBuilder())
				.append(sender)
				.append(LoggerUtil.SEPARATOR)
				.append(receiver)
				.append(LoggerUtil.SEPARATOR)
				.append(counter)
				.append(LoggerUtil.SEPARATOR)
				.append(MessageType.DATA.equals(msgType) ? "DATA" : "ACK")
				.append(LoggerUtil.SEPARATOR)
				.append(timestamp)
				.append(LoggerUtil.SEPARATOR)
				.append(lost)
				.append(LoggerUtil.SEPARATOR)
				.append(effectiveTime)
				.toString();
	}

	public static final class Builder {
		private String sender;
		private String receiver;
		private MessageType msgType;
		private Integer counter;
		private Long timestamp;
		private Object body;
		private Long effectiveTime;

		private Builder() {
		}

		public Builder counter(Integer counter) {
			this.counter = counter;
			return this;
		}

		public Builder sender(String sender) {
			this.sender = sender;
			return this;
		}

		public Builder receiver(String receiver) {
			this.receiver = receiver;
			return this;
		}
		
		public Builder msgType(MessageType msgType) {
			this.msgType = msgType;
			return this;
		}

		public Builder body(Object body) {
			this.body = body;
			return this;
		}
		
		public Builder timestamp(Long timestamp) {
			this.timestamp = timestamp;
			return this;
		}
		
		public Builder effectiveTime(Long effectiveTime) {
			this.effectiveTime = effectiveTime;
			return this;
		}

		public Message build() {
			return new Message(this);
		}
	}


}
