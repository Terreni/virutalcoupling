package it.unifi.terreni.model;

import de.tudresden.sumo.cmd.Vehicle;
import de.tudresden.sumo.config.Constants;
import de.tudresden.sumo.objects.SumoPosition2D;
import de.tudresden.sumo.objects.SumoPrimitive;
import de.tudresden.sumo.objects.SumoStringList;
import de.tudresden.sumo.subscription.SubscriptionObject;
import it.unifi.terreni.RBC;
import it.unifi.terreni.simulator.Simulator;
import it.unifi.terreni.utils.MathTrain;
import it.unifi.terreni.utils.MessageType;

public class Train implements BaseTrain{
	private TrainData trainData;
	private TrainData virtualConnected;
	public static Double TIME_REFRESH = 0.5; //ms
	private Double tickNumber = .0;
	private static Double stepLength = 0.1;
	private Double startX = null;
	private Double startY = null;
	private static Double ths = 60.0;
	boolean isHead;
	boolean dfdJunction; //disconnection for different junction

	private CouplingTrain couplingTrain;
	
	private boolean canCoupling;

	public Train(String vehID, boolean isHead) throws Exception {
		trainData = new TrainData(vehID);
		SumoStringList smsl = (SumoStringList) Simulator.doJob(Vehicle.getRoute(vehID));
		trainData.setEndRouteId(smsl.isEmpty() ? null : smsl.get(smsl.size() - 1));
		RBC.getInstance().addTrain(this);
		this.canCoupling = true;
		this.isHead = isHead;
		this.dfdJunction = false;
		
		BaseStation.getInstance().connect(this);
	}

	public boolean isHead() {
		return isHead;
	}
	
	public boolean isCanCoupling() {
		return canCoupling;
	}


	public Double getDistance(Double x, Double y) {
		if(trainData.getxPos() == null || trainData.getyPos() == null || x == null || y == null) return .0;
		return Math.sqrt(Math.pow(trainData.getxPos() - x, 2) + Math.pow(trainData.getyPos() - y, 2));
	}

	public Double getDistance(Train t) {
		if(t == null) return .0;
		return getDistance(t.getxPos(), t.getyPos());
	}

	public Double getDistance(TrainData td) {
		if(td == null) return .0;
		return getDistance(td.getxPos(), td.getyPos());
	}

	public void update(SubscriptionObject so) {
		if(so.variable == Constants.VAR_SPEED) {
			SumoPrimitive sp = (SumoPrimitive) so.object;
			this.trainData.setActualSpeed((Double) sp.val);
		} else if (so.variable == Constants.VAR_POSITION) {
			SumoPosition2D sc = (SumoPosition2D) so.object;
			this.trainData.setxPos(sc.x);
			this.trainData.setyPos(sc.y);

			if(startX == null && startY == null) {
				startX = sc.x;
				startY = sc.y;
			}
		}  else if (so.variable == Constants.VAR_LANE_ID) {
			SumoPrimitive sp = (SumoPrimitive) so.object;
			this.trainData.setLaneId((String) sp.val);
		}
	}

	public Double getActualSpeed() {
		return (this.trainData == null || this.trainData.getActualSpeed() == null) ? .0 : this.trainData.getActualSpeed();
	}


	public void setActualSpeed(Double actualSpeed) {
		this.trainData.setActualSpeed(actualSpeed);
	}


	public Double getxPos() {
		return this.trainData.getxPos();
	}


	public Double getyPos() {
		return this.trainData.getyPos();
	}


	public String getVehID() {
		return this.trainData.getVehID();
	}
	
	public boolean isConnected() {
		return this.trainData.isConnected();
	}
	
	public void setDisconnectionDuration(double disconnectionDuration) {
		this.trainData.setDisconnectionDuration(disconnectionDuration);
	}

	
	public String getLaneId() {
		return this.trainData.getLaneId();
	}


	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + (this.getVehID() != null ? this.getVehID().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj.getClass() != this.getClass()) {
			return false;
		}

		final Train other = (Train) obj;
		if ((this.getVehID() == null) ? (other.getVehID() != null) : !this.getVehID().equals(other.getVehID())) {
			return false;
		}

		return true;
	}


	public void setSpeed(Double speed) {
		try {
			Simulator.setSpeed(getVehID(), speed);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void nextTrainInfo(Object msg) throws Exception {
		if(virtualConnected != null) return;
		
		TrainData trainData = (TrainData) msg;
		
		Double distance = getDistance(trainData);

		if(distance < RBC.getInstance().getSafetyDistance()) {
			setSpeed(.0);
		} else {
			setSpeed(RBC.getInstance().getSpeed());
		}
	}


	public void sendConnectionRequest(Train train) {
		System.out.println(this.getVehID() + " Virtually connected to " + train.getVehID());
		train.setDestTrainInfo(this.getVehID());
		this.virtualConnected = train.getTrainData();
	}


	private void setDestTrainInfo(String vehID) {
		this.trainData.setDestTrainInfo(vehID);
	}

	public void sendDisconnectionRequest() {
		BaseStation.getInstance().disconnect(this);
		if(!isVirtualCoupled()) return;
		System.out.println(this.getVehID() + " Virtually disconnected");
		this.canCoupling = true;
		this.virtualConnected.setDestTrainInfo(null);
		this.virtualConnected = null;
	}
	
	public void sendDisconnectionForDifJunction() {
		this.dfdJunction = true; 
		this.sendDisconnectionRequest();
	}
	
	

	public boolean isDfdJunction() {
		return dfdJunction;
	}

	public void setDfdJunction(boolean dfdJunction) {
		this.dfdJunction = dfdJunction;
	}

	public TrainData getTrainData() {
		return trainData;
	}

	public String getEndRouteId() {
		return trainData.getEndRouteId();
	}


	public void tick() throws Exception {
		tickNumber += stepLength;
		if(tickNumber >= TIME_REFRESH) {
			tickNumber = .0;
			
			if(getVehicleConnected() != null) {
				BaseStation.getInstance().sendMessage(getMessage(getVehicleConnected(), this.trainData));
			}
			
			if(isVirtualCoupled() && !isCouplingTrain()) {
//				System.out.println("TrainA Position:\t" + Math.sqrt(Math.pow(virtualConnected.getxPos() - startX, 2) + Math.pow(virtualConnected.getyPos() - startY, 2)));
//				System.out.println("TrainB Position:\t" + getPosition());
//				System.out.println("Distance:\t" + getDistance(virtualConnected));
				setSpeed(virtualConnected.getActualSpeed());
				if(MathTrain.getMinimalDistance(MathTrain.getDistance(startX, startY, virtualConnected.getxPos(),  virtualConnected.getyPos()), getPosition()) > ths) {
					this.sendDisconnectionRequest();
				}
			}
		}		
		if(isCouplingTrain()) {
			Double speed = couplingTrain.coupling();
			couplingTrain = speed == null ? null : couplingTrain;
			this.setSpeed(speed == null ? trainData.getActualSpeed() : speed);
			
			if(couplingTrain == null) {
				this.sendConnectionRequest(RBC.getInstance().getHeader(this));
			}
		}
		
		if(!isConnected()) {
			this.trainData.reduceDisconnection(stepLength);
			if(trainData.getDisconnectionDuration() == null || trainData.getDisconnectionDuration() <= .0) {
				BaseStation.getInstance().connect(this);
			}
		}
		
		
		if(isVirtualCoupled() && !this.virtualConnected.isConnected()) {// Se il treno � connesso ad un convoglio che ha perso la connessione si disconnette
			this.sendDisconnectionRequest();
		}
	}


	public boolean isVirtualCoupled() {
		return virtualConnected != null;
	}
	public boolean isCouplingTrain() { // fase di accoppiamento
		return couplingTrain != null;
	}
	
	public String getVehicleConnected() {
		return this.trainData.getDestTrainInfo();
	}


	public Double getPosition() {
		return getDistance(startX, startY);
	}

	public void coupling(Double t1, Double t2, Double t3, Double speed1, Double speed2) {
		this.couplingTrain = new CouplingTrain(t1, t2, t3, speed1, speed2);
	}

	
	@SuppressWarnings("unused")
	private Message getAck(Message msg) {
		return Message.builder()
				.sender(msg.getReceiver())
				.receiver(msg.getSender())
				.msgType(MessageType.ACK)
				.body(null)
				.build();
	}

	
	private Message getMessage(String receiver, Object body) {
		return Message.builder()
				.sender(getVehID())
				.receiver(receiver)
				.msgType(MessageType.DATA)
				.body(body)
				.build();
	}

	public static class CouplingTrain {
		private Double t1;
		private Double t2;
		private Double t3;
		private Double speed1;
		private Double speed2;


		public CouplingTrain(Double t1, Double t2,Double t3, Double speed1, Double speed2) {
			super();
			this.t1 = t1 == null ? .0 : t1;
			this.t2 = t2 == null ? .0 : t2;
			this.t3 = t3 == null ? .0 : t3;
			this.speed1 = speed1;
			this.speed2 = speed2;
		}
		public Double coupling() {
			if(t1 > 0) {
				t1 -= Train.stepLength;
				return speed1;
			}
			
			if(t2 > 0) {
				t2 -= Train.stepLength;
				return speed1;
			}

			if(t3 > 0) {
				t3 -= Train.stepLength;
				return speed2;
			}

			return null;
		}
		public Double getT1() {
			return t1;
		}
		public void setT1(Double t1) {
			this.t1 = t1;
		}
		public Double getT2() {
			return t2;
		}
		public void setT2(Double t2) {
			this.t2 = t2;
		}
		public Double getSpeed1() {
			return speed1;
		}
		public void setSpeed1(Double speed1) {
			this.speed1 = speed1;
		}
		public Double getSpeed2() {
			return speed2;
		}
		public void setSpeed2(Double speed2) {
			this.speed2 = speed2;
		}
	}

}
