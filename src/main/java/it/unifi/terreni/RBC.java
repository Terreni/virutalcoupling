package it.unifi.terreni;

import de.tudresden.sumo.objects.SumoPosition2D;
import it.unifi.terreni.model.Train;
import it.unifi.terreni.queue.Queue;
import it.unifi.terreni.queue.Queue.Trains;
import it.unifi.terreni.simulator.Simulator;
import it.unifi.terreni.utils.MathTrain;

public class RBC {
	public static Double TIME_REFRESH = 0.5; //ms
	public static Double LIV_THREE_MIN_DISTANCE = 1200.0;
	private static RBC instance;
	private Queue queue;
	private Double tickNumber;
	private Double stepLength;
	private Double deceleration;
	private Double acceleration;

	private SumoPosition2D junction;

	public static Double minDistance = 600.0;

	private Double maxSpeed;
	private Double speed;
	private Double safetyDistance;

	private RBC() throws Exception {
		tickNumber = 0.0;
		stepLength = 0.1;
		maxSpeed = 25.0; //ms
		speed = 20.0; //ms
		deceleration = 0.5;
		acceleration = 1.0;
		safetyDistance = Math.pow(maxSpeed, 2) / (2 * deceleration); //m 
		junction = null;

		queue = new Queue();
	}

	public void activateJunction() throws Exception {
		junction = Simulator.getJunctionPosition("gneJ1");//gneJ1
	}

	public static RBC getInstance() throws Exception {
		if(instance == null) {
			instance = new RBC();
		}

		return instance;
	}

	public void tick() throws Exception {
		tickNumber += stepLength;
		if(tickNumber >= TIME_REFRESH) {
			tickNumber = .0;
			for (Trains t : this.queue.getList()) {
				procesTick(t.getNextTrain(), t.getTrain()) ;
				Double distanceNextTrain = t.getNextTrain() == null ? Double.MAX_VALUE : t.getTrain().getDistance(t.getNextTrain());
				if(!t.getTrain().isVirtualCoupled() && !t.getTrain().isCouplingTrain() && distanceNextTrain > LIV_THREE_MIN_DISTANCE) {
					t.getTrain().setSpeed(speed);
				}
			}
		}

	}


	private void procesTick(Train trainA, Train trainB) throws Exception {
		if(trainA == null) return;
		
		if(trainB != null && !trainB.isVirtualCoupled() && !trainB.isCouplingTrain() && canCoupling(trainA, trainB)) {// solo se non sono accoppiati!!!!
			Double va = trainA.getTrainData().getActualSpeed();
			Double vb = trainB.getTrainData().getActualSpeed();

			Double t1 = .0;
			Double t2 = .0;
			Double t3 = .0;
			Double vb1 = (vb <= va ) ? maxSpeed : vb;
			if(vb <= va) {
				t1 = MathTrain.getTimeLinearAcceleration(vb, vb1, acceleration);
			}

			t3 = MathTrain.getTimeLinearAcceleration(va, vb1, deceleration);
			Double x0 = (trainA.getPosition() + MathTrain.getPosLinearVelocity(va, t1) + MathTrain.getPosLinearVelocity(va, t3)) - minDistance - (trainB.getPosition() + MathTrain.getPosLinearAcceleration(acceleration, vb, t1) + MathTrain.getPosLinearAcceleration(deceleration, vb1, t3));
			t2 = MathTrain.getTimeLinearVelocity(x0, vb1 - va);

			Double tc = t1 + t2 + t3;

			Double pCoupling = (trainA.getPosition() -  minDistance) + (tc * va);
			System.out.println(trainB.getVehID() + " can coupling with " + trainA.getVehID() + ":\t " + pCoupling);

			//				System.out.println("TrainA Position:\t" + trainA.getPosition());
			//				System.out.println("TrainB Position:\t" + trainB.getPosition());

			trainB.coupling(t1, t2, t3, vb1, va);
		} 

		if(trainB != null && trainB.isVirtualCoupled() && (isNearJunction(trainA) && !trainA.getEndRouteId().equals(trainB.getEndRouteId()))) {
			System.out.println(trainA.getVehID() + " is near junction " + trainB.getVehID() + " will be disconnected");
			trainB.sendDisconnectionRequest();
			this.remove(trainA.getVehID()); 
		}

		if(trainB != null &&  !trainB.isVirtualCoupled() && !trainB.isCouplingTrain() && trainB.getDistance(trainA) <= LIV_THREE_MIN_DISTANCE) {//!canCoupling(trainA, trainB) 
			trainB.setSpeed(isSameLane(trainA, trainB) ? .0 : maxSpeed);
		}
	}

	private boolean isSameLane(Train trainA, Train trainB) {
		return trainB == null ? false : (!trainB.isDfdJunction() && trainB.getLaneId().equals(trainA.getLaneId()));
	}

	private boolean isNearJunction(Train trainA) {
		if(junction == null || trainA == null) return false;

		Double maxDistance = 10.0 * trainA.getActualSpeed();
		Double distance = MathTrain.getDistance(junction.x, junction.y, trainA.getTrainData().getxPos(), trainA.getTrainData().getyPos());
		return distance <= maxDistance;
	}


	public Train getHeader(Train t) {
		return this.queue.findHeadTrains(t);
	}

	private boolean canCoupling(Train trainA, Train trainB) {
		if(trainA == null || trainB == null ) {
			return false;
		}
		
		if(!trainA.isConnected() || !trainB.isConnected()) return false;

		if(!trainB.isCanCoupling())
			return false;

		if(!isSameLane(trainA, trainB))
			return false;

		if(!trainA.isVirtualCoupled() && !trainA.isHead()) 
			return false;

		Double vb = trainB.getTrainData().getActualSpeed();
		Double va = trainA.getTrainData().getActualSpeed();
		Double tc = getTCord(trainA, trainB);

		Double eoAvc = Math.pow(vb, 2) / (2 * deceleration);

		return Math.abs(eoAvc) <= (tc * va);
	}

	private Double getTCord(Train trainA, Train trainB) {

		Double vb = trainB.getTrainData().getActualSpeed();
		Double va = trainA.getTrainData().getActualSpeed();

		Double t1 = .0;
		Double t2 = .0;
		Double t3 = .0;
		Double vb1 = (vb <= va ) ? maxSpeed : vb;
		if(vb <= va) {
			t1 = MathTrain.getTimeLinearAcceleration(vb, vb1, acceleration);
		}

		t3 = MathTrain.getTimeLinearAcceleration(va, vb1, deceleration);
		Double x0 = (trainA.getPosition() + MathTrain.getPosLinearVelocity(va, t1) + MathTrain.getPosLinearVelocity(va, t3)) - minDistance - (trainB.getPosition() + MathTrain.getPosLinearAcceleration(acceleration, vb, t1) + MathTrain.getPosLinearAcceleration(deceleration, vb1, t3));
		t2 = MathTrain.getTimeLinearVelocity(x0, vb1 - va);

		return t1 + t2 + t3;
	}

	public void setStepLength(Double stepLength) {
		this.stepLength = stepLength;
	}

	public Double getSafetyDistance() {
		return safetyDistance;
	}

	public Double getMaxSpeed() {
		return maxSpeed;
	}
	public Double getSpeed() {
		return speed;
	}

	public void addTrain(Train t) {
		if(queue.isEmpty()) {
			t.setSpeed(speed);	
			queue.enQueue(Trains.builder()
					.train(t)
					.nextTrain(null)
					.build());
		} else {
			Trains ts = queue.lastElement();
			queue.enQueue(Trains.builder()
					.train(t)
					.nextTrain(ts.getTrain())
					.build());
		}
	}
	
	public Train getTrain(String id) {
		for (Trains t : this.queue.getList()) {
			if(t.getTrain().getVehID().equals(id))
				return t.getTrain();
		}
		
		return null;
	}
	
	public void remove(String vehID) throws Exception {
		Trains t = queue.deQueue();
		if(!vehID.equals(t.getTrain().getVehID())) {
			throw new Exception(vehID + " not found");
		}

		if(!queue.isEmpty()) {
			queue.firstElement().getTrain().sendDisconnectionRequest();
			queue.firstElement().setHead();
		}
	}
}
